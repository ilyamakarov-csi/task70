import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.testng.Assert;
import org.testng.annotations.Test;


/**
 * Created by IlyaMakarov on 12/22/2016.
 */
public class ForHTMLUnit {
    @Test
    public void homePage() throws Exception {

        WebClient webClient = new WebClient();
        try  {
            webClient.getOptions().setUseInsecureSSL(true);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            HtmlPage page1 = webClient.getPage("https://192.168.100.26/");

            HtmlInput userName = (HtmlInput) page1.getHtmlElementById("Username");
            HtmlInput password = (HtmlInput) page1.getHtmlElementById("Password");
            HtmlButton submitButton = (HtmlButton) page1.getHtmlElementById("SubmitButton");

            // Change the value of the text field
            userName.setValueAttribute("IlyaMakarov");
            password.setValueAttribute("Password1");
            HtmlPage newPage = submitButton.click();

            HtmlPage homePage = (HtmlPage) webClient.getCurrentWindow().getEnclosedPage();
            String text = homePage.getTitleText();
            Assert.assertEquals("RMSys - Home", text);
        }
        finally {

        }
    }

}
