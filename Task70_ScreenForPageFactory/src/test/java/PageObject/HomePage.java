package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class HomePage {
    WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public LoginPage signOut() {
        driver.findElement(By.cssSelector(".sign-out-span>a")).click();
        return new LoginPage(driver);
    }

    public boolean isSignOutButtonVisible() {
        return driver.findElement(By.cssSelector(".sign-out-span>a")).isDisplayed();
    }
}
