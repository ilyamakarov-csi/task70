package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class LoginPage {

    WebDriver driver;
    final String address = "https://192.168.100.26/";

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        driver.get(address);
    }

    public HomePage login(String login, String password) {
        driver.findElement(By.id("Username")).sendKeys(login);
        driver.findElement(By.id("Password")).sendKeys(password);
        driver.findElement(By.id("SubmitButton")).click();

        return new HomePage(driver);
    }

    public boolean isSubmitButtonDisplayed() {
        return driver.findElement(By.id("SubmitButton")).isDisplayed();
    }
}
