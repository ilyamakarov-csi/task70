package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class TestHomePage {

    WebDriver driver;

    @BeforeClass
    public void setUp() {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public void tearDown() {
        //driver.quit();
    }

    @Test
    public void logoutTest() {
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.login("IlyaMakarov", "Password1");
        loginPage = homePage.signOut();
        Assert.assertTrue(loginPage.isSubmitButtonDisplayed());
    }
}
