package PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class LoginPage {

    WebDriver driver;
    final String address = "https://192.168.100.26/";

    @FindBy(how = How.ID, using = "Username")
    WebElement login;

    @FindBy(how = How.ID, using = "Password")
    WebElement password;

    @FindBy(how = How.ID, using = "SubmitButton")
    WebElement submitButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        driver.get(address);
    }

    public HomePage login(String login, String password) {
        this.login.sendKeys(login);
        this.password.sendKeys(password);
        submitButton.click();

        return PageFactory.initElements(driver, HomePage.class);
    }

    public boolean isSubmitButtonDisplayed() {
        return submitButton.isDisplayed();
    }
}
