package PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class HomePage {

    WebDriver driver;

    @FindBy(how=How.CSS, using=".sign-out-span>a")
    WebElement signOut;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public LoginPage signOut() {
        signOut.click();
        return PageFactory.initElements(driver, LoginPage.class);
    }

    public boolean isSignOutButtonVisible() {
        return signOut.isDisplayed();
    }
}
